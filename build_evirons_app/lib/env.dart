import 'package:flutter/material.dart';
import 'package:build_evirons_app/main.dart';

class Env {

  static Env value;

  String baseUrl;
  MaterialColor primarySwatch;

  Env() {
    value = this;
    runApp(MyApp(this));
  }

  String get name => runtimeType.toString();
}