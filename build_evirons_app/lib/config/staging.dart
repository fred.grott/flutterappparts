import 'package:build_evirons_app/env.dart';
import 'package:flutter/material.dart';

void main() => Staging();

class Staging extends Env {
  @override
  final String baseUrl = 'https://api.staging.website.org';
  @override
  final MaterialColor primarySwatch = Colors.amber;
}