import 'package:build_evirons_app/env.dart';
import 'package:flutter/material.dart';

void main() => Production();

class Production extends Env {
  @override
  final String baseUrl = 'https://api.website.org';
  @override
  final MaterialColor primarySwatch = Colors.teal;
}