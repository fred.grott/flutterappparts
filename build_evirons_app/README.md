# build_evirons_app
A better way to implement build variants via multi-app-entry points in
my humble way.

# Implementation

Template pattern to differeitate main functions per build variant with the full 
env object passed to MyApp class via constructor.

One gitignore the config dart classes except for the one used for CIServers.

![dev](/screenshots/development.png)

![prod](/screenshots/production.png)

![staging](/screenshots/staging.png)

# Credits

I found RoTGP's example when tracking down whether we could do environment vars via commandlline with the 
build system, see:

https://github.com/ROTGP/flutter_environments

I did some minor Effective Dart cleanup and changed the Color ref to the more correct 
MaterialColor type ref.


# license

BSD clause 2 copyright 2019 Fredrick Alllan Grott

