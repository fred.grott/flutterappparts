import 'package:fimber_catcher_mystery_app/env.dart';
import 'package:flutter/material.dart';

void main() => Testing();

class Testing extends Env {
  @override
  final String baseUrl = 'https://api.testing.website.org';
  @override
  final MaterialColor primarySwatch = Colors.lime;
}