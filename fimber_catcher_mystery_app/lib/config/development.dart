import 'package:fimber_catcher_mystery_app/env.dart';
import 'package:flutter/material.dart';

void main() => Development();

class Development extends Env {
  @override
  final String baseUrl = 'https://api.dev.website.org';
  @override
  final MaterialColor primarySwatch = Colors.pink;
}