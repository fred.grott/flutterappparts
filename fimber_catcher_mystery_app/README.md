# fimber_catcher_mystery_app

fimber logging and application exceptiopn catching with catcher setup with 
build variants.

# Implementation

staging build variant is gitignored as its usually CIserver executed.

# Resources

[fimber](https://github.com/magillus/flutter-fimber)

[catcher](https://github.com/jhomlala/catcher)

# License

BSD clause 2 copyright 2019 Fredrick Allan Grott